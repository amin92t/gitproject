let now_playing = document.querySelector(".now_playing");
let track_art = document.querySelector(".track_art");
let track_name = document.querySelector(".track_name");
let track_artist = document.querySelector(".track_artist");

let playpause_trackBtn = document.querySelector(".playpause_track");
let prev_track = document.querySelector(".prev_track");
let next_track = document.querySelector(".next_track");

let current_time = document.querySelector(".current_time");
let total_duration = document.querySelector(".total_duration");
let seek_slider = document.querySelector(".seek_slider");
let volume_slider = document.querySelector(".volume_slider");

let track_Index = 0;
let isPlaying = false;


let curr_Track = document.createElement("audio");

let track_List = [
    {
        name: "Pichak1",
        artist: "Ragheb1",
        image: "pic/image-asset.jpeg",
        path: "Ragheb - Pichak (128).mp3"
    },
    {
        name: "Pichak2",
        artist: "Ragheb2",
        image: "pic/image-asset.jpeg",
        path: "Ragheb - Pichak (128).mp3"
    },
    {
        name: "Pichak3",
        artist: "Ragheb3",
        image: "pic/image-asset.jpeg",
        path: "Ragheb - Pichak (128).mp3"
    }

]

function random_bg_color() {
    let red = Math.floor(Math.random() * 255);
    let blue = Math.floor(Math.random() * 255);
    let green = Math.floor(Math.random() * 255);

    let bgColor = "rgb(" + red + "," + green + "," + blue + ")";
    document.body.style.background = bgColor;
}

function seekUpdate() {
    let seekPosition = 0;
    if (!isNaN(curr_Track.duration)) {
        seekPosition = curr_Track.currentTime * (100 / curr_Track.duration);

        seek_slider.value = seekPosition;

        let currentMinutes = Math.floor(curr_Track.currentTime / 60);
        let currentSeconds = Math.floor(curr_Track.currentTime - currentMinutes * 60);
        let durationMinutes = Math.floor(curr_Track.duration / 60);
        let durationSeconds = Math.floor(curr_Track.duration - durationMinutes * 60);

        if (currentSeconds < 10) { currentSeconds = "0" + currentSeconds; }
        if (durationSeconds < 10) { durationSeconds = "0" + durationSeconds; }
        if (currentMinutes < 10) { currentMinutes = "0" + currentMinutes; }
        if (durationMinutes < 10) { durationMinutes = "0" + durationMinutes; }

        current_time.textContent = currentMinutes + ":" + currentSeconds;
        total_duration.textContent = durationMinutes + ":" + durationSeconds;
    }
}

function loadTrack(track_Index) {

    curr_Track.src = track_List[track_Index].path; //1
    curr_Track.load(); //2
    track_art.style.backgroundImage = "url(" + track_List[track_Index].image + ")"; //3
    track_name.textContent = track_List[track_Index].name; //4
    track_artist.textContent = track_List[track_Index].artist; //5
    now_playing.textContent = "Playing" + (track_Index + 1) + "OF" + track_List.length; //6

    updateTimer = setInterval(seekUpdate, 1000); //7

}


//2
function playTrack() {
    curr_Track.play();
    isPlaying = true;
    playpause_trackBtn.innerHTML = '<i class="fa fa-play-circle fa-5x"></i>';
}
function pauseTrack() {
    curr_Track.pause();
    isPlaying = false;
    playpause_trackBtn.innerHTML = '<i class="fa fa-pause-circle fa-5x"></i>';
}
//1
function playpauseTrack() {
    if (!isplaying) playTrack();
    else pauseTrack();
}

function nextTrack() {
    if (track_Index < track_List.length - 1) {
        track_Index += 1;
    } else track_Index = 0;
    loadTrack(track_Index);
    playTrack();
}
function prevTrack() {
    if (track_index > 0)
        track_index -= 1;
    else track_index = track_list.length;
    loadTrack(track_index);
    playTrack();
}




