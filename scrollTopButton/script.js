function smoothScrollTo(yPos) {
    var step = 20;
    if (yPos < scrollY) {
        step *= -1;
    }
    if (Math.abs(yPos - scrollY) <= step) { // stop Condition
        return;
    }
    window.scrollBy(0, step);
    setTimeout(function () {
        smoothScrollTo(yPos)
    }, 3);
}


function myFadeIn(elem) {
    var elemOpacity = Number(getComputedStyle(elem).opacity).toPrecision(2);
    elem.style.opacity = elemOpacity;
    if (elemOpacity >= 1) {
        return;
    }
    elem.style.opacity = Number(elemOpacity) + 0.01;
    setTimeout(function () {
        myFadeIn(elem)
    }, 10)
}

function myfadeOut(elem){
    let elemOpacity = Number(getComputedStyle(elem).opacity).toPrecision(2);
    elem.style.opacity = elemOpacity ;
    if(elemOpacity <= 0){
        return;
    }
    elem.style.opacity = Number(elemOpacity) - 0.1 ;
    setTimeout(function(){
        myfadeOut(elem)
    }, 50)
 }

Element.prototype.fadeout =function(){
    myfadeOut(this);
}
Element.prototype.fadein = function () {
    myFadeIn(this);
}