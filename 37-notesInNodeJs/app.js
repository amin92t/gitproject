

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');
const notes = require('./notes');


let argv = yargs.argv;
let command = argv._[0];

console.log('process :' + process.argv);
console.log('argv :' + argv);
console.log('command :' + command);

if (command === 'add') {
    let note = notes.addNote(argv.title, argv.body);
    if (note) {
        console.log('note created...!');
        notes.logNotes(note);
    } else {
        console.log('Note title taken');
    }
} else if (command === 'list') {
    let allNotes = notes.getNotes();
    console.log(`${allNotes.length} - notes:`);
    allNotes.forEach(note => {
        notes.logNotes(note);
    });

} else if (command === 'read') {
    let note = notes.getNote(argv.title);
    if (note) {
        console.log('Note Found!');
        notes.logNotes(note);
    } else {
        console.log('Note not found');
    }
} else if (command === 'remove') {
    let note = notes.removeNote(argv.title);
    let message = note ? 'Note was Removed' : 'Not not found';
    console.log(message);

} else {
    console.log('عدم دسترسی');
}