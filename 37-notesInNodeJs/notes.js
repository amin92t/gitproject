const fs = require('fs');

var fetchNotes = ()=> {
    try {
        let noteString = fs.readFileSync('notes-data.json');//2
        return JSON.parse(noteString);    //2
    } catch (error) {
        return [];
    }
}

var saveNotes = (notes)=>{
    fs.writeFileSync('notes-data.json',JSON.stringify(notes)); //1
}

let addNote = (title, body) => {
    let notes = fetchNotes();
    let note = {
        title,
        body
    }

    let duplicateNote = notes.filter((note) => note.title === title);//3
    if (duplicateNote.length == 0) {
        notes.push(note); //1
        saveNotes(notes);
        return note ;
    }
}

let getNotes = () => {
    return fetchNotes();
}
let getNote = (title) => {
    let notes = fetchNotes();
    let filterNotes = notes.filter((note)=>note.title === title);
    return filterNotes[0];
}
let removeNote = (title) => {
    let notes = fetchNotes();
    let filterNotes = notes.filter((note)=> note.title != title);
    saveNotes(filterNotes);
    return notes.length != filterNotes.length;
}

let logNotes = (note) =>{
    console.log('-------')
    console.log(`Title ${note.title}`);
    console.log(`Body ${note.body}`);
}

module.exports = {
    addNote,
    getNote,
    getNotes,
    removeNote,
    logNotes
}