let display = document.getElementById('display');
let btn = document.getElementById('btn');
let divideBtn = document.getElementById('divide');
let multiplyBtn = document.getElementById('multiply');
let minusBtn = document.getElementById('minus');
let plusBtn = document.getElementById('plus');
let clearBtn = document.getElementById('clear');
let rootBtn = document.getElementById('root');
let squareBtn = document.getElementById('square');
let backspaceBtn = document.getElementById('backspace');
let equalBtn = document.getElementById('equal');

let memory = 0;
let lastOperation = '' ;

    function inputNumber(event){
        if(display.textContent.length > 10){
            return ;
        }
        let data = event.target.dataset.input ;
        if (data) {
            if(data === '.'){
                if(!display.textContent.includes('.')){
                    display.textContent += data ;
                }
            }else{
                display.textContent += data ;
                if (!display.textContent.includes('.')){
                    display.textContent = Number(display.textContent);
                }
            }
        }
    }

btn.addEventListener('click',inputNumber);
clearBtn.addEventListener('click', () => {
    display.textContent = 0 ;
    lastOperation = '';
    memory = 0 ;
});

minusBtn.addEventListener('click', () => {
    lastOperation = 'minus';
    memory = Number(display.textContent);
    display.textContent = 0 ;
});

plusBtn.addEventListener('click', () => {
    lastOperation = 'plus';
    memory = Number(display.textContent);
    display.textContent = 0 ;
});

divideBtn.addEventListener('click', () => {
    lastOperation = 'division';
    memory = Number(display.textContent);
    display.textContent = 0 ;
});

multiplyBtn.addEventListener('click', () => {
    lastOperation = 'multiply';
    memory = Number(display.textContent);
    display.textContent = 0 ;
});

equalBtn.addEventListener('click', () => {
    if (!lastOperation == '') {
        let num = Number(display.textContent);
        if (lastOperation == 'minus') {
            display.textContent = memory - num ;
        } else if (lastOperation == 'plus') {
            display.textContent = memory + num ;
        } else if (lastOperation == 'divisio') {
            display.textContent = memory / num ;
        } else if (lastOperation == 'multiply') {
            display.textContent = memory * num ;
        }
        lastOperation = '' ;
    }
});

backspaceBtn.addEventListener('click', () => {
    if (display.textContent.length ==1) {
        display.textContent = 0 ;
    }else{
        display.textContent = display.textContent.substring(0, display.textContent.length-1);
    }
});

squareBtn.addEventListener('click', ()=> {
    display.textContent **=2 ;
    lastOperation = ''
});

rootBtn.addEventListener('click', ()=>{
    display.textContent = Math.sqrt(display.textContent);
    lastOperation = '' ;
}) ;

