(function ($) {
    $.fn.pcModal = function (userOptions) {
        let options = $.extend({
            wrapperBgColor : "#000",
            wrapperOpacity : 0.8,
            width : 500,
            entrance : "top",
            speed : 500 ,
            top : 100 ,
            showEvent : "click" ,
            showCloseButton : "false" , //Tamrin
            CloseButtonText : "X" ,
            onload : function () {

            },
            onUnload : function () {

            }
        },userOptions);

        let enterModal = function(modal, enterMode){
            var wh = $(window).height();
            var ww = $(window).width();
            var topPosition = options.top + "px";
            var leftPosition = (ww - modal.width()) / 2 + "px";

            switch (enterMode) {
                case "fade" :
                    modal.css({top:topPosition,left:leftPosition})
                    modal.fadeIn(options.speed)
                    break;
                case "bottom":
                    modal.css({top:2 * wh ,left:leftPosition,display:"block"});
                    modal.animate({top:topPosition,left:leftPosition},options.speed);
                    break;
                case "left":
                    modal.css({top:topPosition ,left:-1 * ww ,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
                case "right":
                    modal.css({top:topPosition ,left:2 * ww ,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
                case "topleft":
                    modal.css({top:-1 * wh ,left:-1 * ww ,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
                case "topright":
                    modal.css({top:-1 * wh ,left:2 * ww ,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
                case "bottomleft":
                    modal.css({top:2 * wh ,left:-1 * ww ,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
                case "bottomright":
                    modal.css({top:2 * wh ,left:2 * ww ,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
                case "top":
                default :
                    modal.css({top:-1 * wh ,left:leftPosition,display:"block"})
                    modal.animate({top:topPosition,left:leftPosition},options.speed)
                    break;
            }
        };

        $(document).ready(function () {
            let modalButtons = $("a[data-modal],button[data-modal]");
            let wrapper = $("<div>").addClass("slModalWrapper").css({
                backgroundColor : options.wrapperBgColor ,
                opacity : options.wrapperOpacity
            });
            modalButtons.each(function () {
                let mBtn = $(this);
                let mBox = $("#"+mBtn.attr("data-modal")).css({width: options.width + "px"});
                mBtn.click(function (ev) {
                   ev.preventDefault() ;
                   mBox.before(wrapper);
                   if (typeof options.onload === "function"){
                       options.onload();
                   }
                   wrapper.fadeIn(options.speed);
                   let enterMode = mBox.is("[data-entrance]")?mBox.attr("data-entrance"):options.entrance;
                   enterModal(mBox,enterMode);


                   wrapper.click(function () {
                        wrapper.fadeOut(options.speed);
                        mBox.fadeOut(options.speed,function () {
                            if (typeof options.onUnload === "function"){
                                options.onUnload();
                            }
                        });
                   })
               })
            })
        })
    }
})(jQuery)