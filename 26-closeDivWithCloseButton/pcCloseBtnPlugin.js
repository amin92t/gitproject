(function ($) {
    $.fn.pcClosable = function (userOptions) {
        let options = $.extend({
            animation : "fadeOut",
            closeIconURL : ""
        },userOptions);
        this.each(function () {
            let elem = $(this);
            let pos = elem.css("position");
            if (pos == "static" || !pos ){
                elem.css({position:"relative"});
            }
            let closeBtn = $("<span>").attr("class","pCloseBtn");
            closeBtn.click(function () {
               // $(this).parent().hide();
                switch (options.animation) {
                    case "fadeOut":
                        $(this).parent().fadeOut();
                        break;
                    case "hide":
                        $(this).parent().hide();
                        break;
                    case "slideUp":
                        $(this).parent().slideUp();
                        break;
                    default :
                        $(this).parent().hide();
                }
            });
            if (options.closeIconURL){
                closeBtn.css("background","url("+options.closeIconURL+")");
            }
            elem.append(closeBtn);
        })
    }
}(jQuery))