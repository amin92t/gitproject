(function ($) {
    $.fn.slSlider = function (userOptions) {
        var options = $.extend({
            slideTime: 5000,                // how long each slide stays on screen
            speed: 500,                     // slide transition speed
            outAnimation: "random",         // swipeRight,swipeLeft,slide,fade,random
            inAnimation: "random",          // swipeRight,swipeLeft,slide,fade,random
            pauseOnHover: true,             // pause when hovering with the mouse
            swipeSupport: true,             // support for swipe gestures (need jq.mobile)
            displayOrder: "sequential",     // sequential,random
            showCounter: true,              // slide page number at top left
            showCaptions: true,             // show slide Caption
            showNextPrev: true,             // show previous/next arrows
            showControls: true              // display slide number buttons
        }, userOptions);



        let slider = $(this);
        let slides = slider.find("ul").children();
        let slideCount = slides.length;
        let i = 0; // current slide index

        $(document).ready(function () {

            // Counter Tag
            let counterTag = $("<div>").addClass("counter");
            if (options.showCounter == true) {
                slider.append(counterTag);
                counterTag.html((i + 1) + " / " + slideCount);
            }

            // add caption slides
            let captionTag = $("<div>").addClass("sliderCaption");
            if (options.showCaptions) {
                slider.append(captionTag);
                if ($(slides[i]).is("[title]")) {
                    captionTag.html($(slides[i]).attr("title")).fadeIn(options.speed)
                }
            }

            if (options.showNextPrev){
                let topDistanse = slider.height()/2 - 12 ;
                let nextTag = $("<span>").css({top:topDistanse}).addClass("next");
                let prevTag = $("<span>").css({top:topDistanse}).addClass("prev");
                slider.append(nextTag,prevTag);
                nextTag.click(function () {
                    showSlide(i+1);
                })
                prevTag.click(function () {
                    showSlide(i-1);
                })
            }

            if (options.showControls){
                let buttonsRowTags = $("<div>").addClass("buttonsRow");
                for (let j = 0 ; j<slideCount ; j++)
                    buttonsRowTags.append($("<span>").attr("data-sn",j).html(j+1));
                slider.append(buttonsRowTags);
                slider.find("div.buttonsRow span").click(function () {
                    let sn = Number($(this).attr("data-sn"));
                    if (sn != i)
                        showSlide(sn);
                });
            }

            let iv = setInterval(autoSlide,options.slideTime);
            function autoSlide() {
                showSlide(i+1);
            }

            function  showSlide(s) {
                $(slides[i]).fadeOut();
                i = (s + slideCount) % slideCount ;
                $(slides[i]).fadeIn();

                if (options.showCaptions){
                    if ($(slides[i]).is("[title]")){
                        captionTag.html($(slides[i]).attr("title")).fadeIn(options.speed);
                    }else {
                        captionTag.html("").fadeOut(options.speed);
                    }
                }

                if (options.showCounter)
                    counterTag.text(i + 1 + " / " + slideCount);

                if (options.showControls){
                    slider.find(".buttonsRow span").removeClass("curr");
                    slider.find(".buttonsRow span:eq("+ i +")").addClass("curr");
                }
            }


        })

    }
})(jQuery)