(function ($) {
    $.fn.pctoolTip = function (userOptions) {
        let settings = $.extend({
            width : 200
        },userOptions);
        this.each(function () {
            let elem = $(this);
            let ttWidth = settings.width ;
            if (elem.is("[data-ttw]")){
                ttWidth = elem.attr("data-ttw");
            }

            let ttBox = $("<div>").attr("class","tooltipBox").css({width: ttWidth + "px"}) ;
            if (elem.is("[data-ttbg]")){
                ttBox.css("backgroundColor",elem.attr("data-ttbg"));
            }
            elem.hover(function () {
                if($(this).is("[title]")){
                    ttBox.html($(this).attr("title"));
                    $(document.body).append(ttBox);
                    $(this).data('titleData',$(this).attr("title"));
                    $(this).removeAttr('title');
                    let elemOffset = $(this).offset();
                    let ttBoxBottom = $(window).height() - elemOffset.top + 10;
                    let ttBoxleft = elemOffset.left - ttWidth/2 ;
                    ttBox.css({bottom:ttBoxBottom,left:ttBoxleft});
                    ttBox.fadeIn();
                }
            },function () {
                    $(this).attr("title",$(this).data("titleData"));
                    ttBox.fadeOut(1000,function () {
                        ttBox.remove();
                    });
            })
        })
    }
})(jQuery);

