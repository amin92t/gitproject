let input1 = document.getElementById('maximum');
let input2 = document.getElementById('guess-Number');
let btn1 = document.getElementById('start');
let btn2 = document.getElementById('guessBtn');
let remainGuess = document.getElementById('guess-Remaining');
let selectedNumber = document.getElementById('selectNumber');
let stage1 = document.getElementById('stage1');
let stage2 = document.getElementById('stage2');
let statusPar = document.getElementById('status');

let maximumNumber , targetNumber , countGuess;


function startGame(){
    maximumNumber = Number(input1.value);
    if (maximumNumber < 10) {
        selectedNumber.innerHTML = '<p class="blue">عدد وارد شده کوچک است</p>';
        return ;
    }else{
        targetNumber = Math.floor(Math.random()* (maximumNumber + 1));
        countGuess = Math.floor(Math.log2(maximumNumber)) + 1 ;
        remainGuess.textContent = countGuess ;
        stage1.classList.toggle('hidden');
        stage2.classList.toggle('hidden');
    }
}

function guess(){
    let yourGuess = Number(input2.value);
    if (yourGuess == targetNumber) {
        stage2.innerHTML = '<p style="color:blue">شما موفق شدید</p>';
        return ;
    }
    if (yourGuess > targetNumber) {
        statusPar.innerHTML += `<span class="red">${yourGuess}</span>`;
    }else{
        statusPar.innerHTML += `<span class="blue">${yourGuess}</span>`;
    }
    countGuess = Number(remainGuess.textContent);
    countGuess--;
    if (countGuess > 0) {
        remainGuess.innerText = countGuess;
    } else {
        stage2.innerHTML = "تعداد نوبت بازی شما به اتمام رسیده است";
    }
}
btn1.addEventListener('click', startGame);
btn2.addEventListener('click', guess);
