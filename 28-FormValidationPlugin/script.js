(function($){
    $.fn.pcValidate = function(userOptions){
        let options = $.extend({
            errorDivClass : "errorMsg",
            fieldNameAttr : "name",
            errorPosition : "prepend"
        },userOptions);

        let emailRegex = /([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})/i;
        let urlRegex = /(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/;
        let requiredRegex = /([^\s]+)/;

        let startWith = function(str,prefix){
            return (str.slice(0,prefix.length) == prefix);
        }

        this.each(function(){
            let frm = $(this) ;
            let errorDiv = $("<div>").addClass(options.errorDivClass);

            frm.submit(function(ev){
                let errors = [] ;
                let fields = $(this).find("input[data-vld],textarea[data-vld]");

                fields.each(function(){
                    let field = $(this);
                    let vldString = field.attr("data-vld").split("|");
                    for (let i in vldString) {
                        let vldStr = vldString[i].trim();
                        if (vldStr === "required") {
                            if (!requiredRegex.test(field.val())) {
                                errors.push(" -Field " + field.attr(options.fieldNameAttr) + "is required");
                                if (!field.next().is("span.required")) {
                                    field.after("<span class='required'>*</span>");
                                }else{
                                    field.next("span.required").remove(); //*
                                }
                            }
                        }else if(vldStr === "email"){
                            if (!emailRegex.test(field.val())) {
                                errors.push(" -Field " + field.attr(options.fieldNameAttr) + "is required");
                                if (!field.next().is("span.required")) {
                                    field.after("<span class='required'>*</span>");
                                }
                            }
                        }else if(vldStr === "url"){
                            if (!urlRegex.test(field.val())) {
                                errors.push(" -Field " + field.attr(options.fieldNameAttr) + "is required");
                                if (!field.next().is("span.required")) {
                                    field.after("<span class='required'>*</span>");
                                }
                        }else if(startWith(vldStr,"minlen_")){

                        }else if(startWith(vldStr,"maxlen_")){
                            let len = Number(vldStr.split("_")[1]);
                            if (field.val().length < len) {
                                errors.push(" -Field " + field.attr(options.fieldNameAttr) + "is" + len + "required");
                            }

                        }else{
                            //invalid vldstring
                            alert(vldStr + "is not valid string");
                        }
                    }
                }
                })

                if (errors.length == 0) {
                    return true ;
                }else{
                    ev.preventDefault();
                    errorDiv.html(errors.join("<br>"));
                    frm.prepend(errorDiv);
                }
            })
        })

    }
})(jQuery)
