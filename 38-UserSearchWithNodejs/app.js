const http = require('http');
const url = require('url');
const searchModule = require('./search');

let server = http.createServer((request, response)=>{
    let requestURL = url.parse(request.url);
    if (request.method == 'GET') {
        if (requestURL.pathname == '/') {
            response.end('Hello');
        }
        if (requestURL.pathname == '/info') {
            let queryparts = requestURL.query.split('=')
               searchModule.getUserInfoByName(queryparts[1], function(userInfo){
                   if (userInfo) {
                       response.end(userInfo);
                   }
               });
        } 
    }
})  

server.listen(2800);