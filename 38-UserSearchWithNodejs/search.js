    const fs = require('fs');

    const Search = function(){};
    // ()=>{} not working

    Search.prototype.getUserInfoByName = (name , cb)=>{
        fs.readFile('./people-data.txt', 'utf8', (error, data)=>{
          //  console.log(data);
          let userInfo = null ;
          let people = data.split('\n');
          name = name.toLowerCase(); 
          people.map((personData)=>{
              let lowerCasePersonData = personData.toLowerCase();
              if (lowerCasePersonData.indexOf(name) >= 0) {
                  userInfo = personData ;
              }
          })
          cb(userInfo);
        });

    }

    module.exports = new Search ;