let time = document.getElementById('time');
let date = document.getElementById('date');
let timeString = '' ;

function changeNumber(num){
    let fa_Digit = ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'];
    let output = '' ;

    for (let i = 0; i < num.length; i++) {
        if (num[i]>=0 && num[i]<=9) {
            output += fa_Digit[num[i]];
        }else{
            output += num[i];
        }
    }
    return output ;
}

function update(){
    let timedate = new Date();
    let seconds = timedate.getSeconds();
    let minutes = timedate.getMinutes();
    let hours = timedate.getHours();
    timeString = `${seconds}:${minutes}:${hours}`;
    time.innerHTML = changeNumber(timeString);

    let weekDay = timedate.toLocaleDateString('fa-IR', {weekday:'long'});
    let monthDay = timedate.toLocaleDateString('fa-IR', {day:'numeric'});
    let year = timedate.toLocaleDateString('fa-IR', {year:'numeric'});
    let monthName = timedate.toLocaleDateString('fa-IR', {month:'long'});

    }

setInterval(update, 1000);